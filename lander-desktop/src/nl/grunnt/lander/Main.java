package nl.grunnt.lander;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "lander";
		cfg.useGL20 = false;
		cfg.width = 800;
		cfg.height = 600;
		cfg.samples = 4;
		cfg.depth = 0;
		new LwjglApplication(new FrontierLander(), cfg);
	}
}
