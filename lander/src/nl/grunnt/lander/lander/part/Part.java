package nl.grunnt.lander.lander.part;

import nl.grunnt.lander.controller.LanderController.Control;
import nl.grunnt.lander.particle.ParticleGenerator;
import box2dLight.Light;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;

public class Part {

	public TextureRegion texture;
	public Body body;
	public float textureDeltaX, textureDeltaY;

	public boolean active;
	public float fuel;

	public PartDesign design;

	public Control control;
	public Array<Part> fuelSources = new Array<Part>();

	public ParticleGenerator particleGen;
	public Light light;
}
