package nl.grunnt.lander.lander.design;

import nl.grunnt.lander.lander.LanderBuilder.Angle;

public class LanderDesignAttach {
	public int attachPoint;
	public LanderDesignPart otherPart;
	public int otherAttachPoint;
	public Angle attachAngle;
	public boolean fuelSource;
}
