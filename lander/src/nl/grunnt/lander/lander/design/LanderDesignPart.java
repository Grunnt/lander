package nl.grunnt.lander.lander.design;

import com.badlogic.gdx.utils.Array;

import nl.grunnt.lander.controller.LanderController.Control;
import nl.grunnt.lander.lander.LanderBuilder.Angle;

public class LanderDesignPart {
	public String partName;
	public Array<LanderDesignAttach> attached = new Array<LanderDesignAttach>();
	public Control control;

	public LanderDesignPart control(Control control) {
		this.control = control;
		return this;
	}

	public LanderDesignPart attach(String partName, int attachPoint, int otherAttachPoint, Angle angle,
			boolean fuelSource) {
		LanderDesignAttach newAttach = new LanderDesignAttach();
		newAttach.attachPoint = attachPoint;
		newAttach.otherAttachPoint = otherAttachPoint;
		newAttach.attachAngle = angle;
		newAttach.otherPart = new LanderDesignPart();
		newAttach.otherPart.partName = partName;
		newAttach.fuelSource = fuelSource;
		attached.add(newAttach);
		return newAttach.otherPart;
	}
}
