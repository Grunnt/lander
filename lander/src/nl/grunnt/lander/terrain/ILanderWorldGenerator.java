package nl.grunnt.lander.terrain;

import com.badlogic.gdx.physics.box2d.*;

public interface ILanderWorldGenerator {

	public LanderWorld generate(World world, float widthM);

}
