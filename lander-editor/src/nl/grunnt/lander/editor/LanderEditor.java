package nl.grunnt.lander.editor;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import net.miginfocom.swing.MigLayout;
import nl.grunnt.lander.editor.EditorPanel.EditorState;
import nl.grunnt.lander.lander.part.PartDesign;

import com.badlogic.gdx.utils.Json;
import javax.swing.JCheckBox;
import javax.swing.JLabel;

/**
 * Copyright (c) 2013, Bas van Schoonhoven. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met: Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or other materials provided with the
 * distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

@SuppressWarnings("serial")
public class LanderEditor extends JFrame {

	public static final String PART_DESIGNS_FILE = "../lander-android/assets/data/parts.json";

	PartDesign[] partDesigns;
	HashMap<String, BufferedImage> partImages;

	PartDesign currentPartDesign = null;

	public LanderEditor() {
		// Select frame
		// Set grid
		// Set hitbox boundaries (polygon)
		// Save to json
		try {

			setTitle("Lander Editor");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setPreferredSize(new Dimension(640, 480));

			loadImages();

			Json json = new Json();

			// Load part designs
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(
					new File(PART_DESIGNS_FILE))));
			partDesigns = json.fromJson(PartDesign[].class, br);
			br.close();
			getContentPane().setLayout(new MigLayout("", "[624px,grow]", "[grow][442px][grow]"));

			JPanel panel = new JPanel();
			getContentPane().add(panel, "cell 0 0 1 2,grow");
			panel.setLayout(new MigLayout("", "[83px][119px][55px][55px][][grow]", "[23px][grow]"));

			final EditorPanel editorPanel = new EditorPanel();

			final JCheckBox chckbxSnapToGrid = new JCheckBox("Snap to grid");
			chckbxSnapToGrid.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					editorPanel.setSnapToGrid(chckbxSnapToGrid.isSelected());
				}
			});
			panel.add(chckbxSnapToGrid, "cell 0 0,alignx left,aligny top");

			final JCheckBox chckbxSnapToSprite = new JCheckBox("Snap to sprite edge");
			chckbxSnapToSprite.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					editorPanel.setSnapToSprite(chckbxSnapToSprite.isSelected());
				}
			});
			panel.add(chckbxSnapToSprite, "cell 1 0,alignx left,aligny top");
			
						JLabel lblNewLabel = new JLabel("Grid size");
						panel.add(lblNewLabel, "cell 2 0,alignx trailing");
			
						final JComboBox<Float> comboBox = new JComboBox<Float>();
						comboBox.addItem(0.025f);
						comboBox.addItem(0.05f);
						comboBox.addItem(0.1f);
						comboBox.addItem(0.2f);
						comboBox.setSelectedIndex(2);
						comboBox.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent ae) {
								editorPanel.setGridSize((Float) comboBox.getSelectedItem());
							}
						});
						panel.add(comboBox, "cell 3 0,growx");

			panel.add(editorPanel, "cell 0 1 6 1,grow");

			JPanel controlPanel = new JPanel();
			getContentPane().add(controlPanel, "cell 0 2,grow");

			final JComboBox<PartDesign> partsComboBox = new JComboBox<PartDesign>();
			partsComboBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					currentPartDesign = (PartDesign) partsComboBox.getSelectedItem();
					editorPanel.setDesign(partImages.get(currentPartDesign.textureName), currentPartDesign);
					pack();
				}
			});
			for (PartDesign partDesign : partDesigns) {
				partsComboBox.addItem(partDesign);
			}
			partsComboBox.setSelectedIndex(0);
			controlPanel.add(partsComboBox);

			JRadioButton rdbtnShape = new JRadioButton("Shape");
			rdbtnShape.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					editorPanel.setState(EditorState.Shape);
				}
			});
			controlPanel.add(rdbtnShape);

			JRadioButton rdbtnAttach = new JRadioButton("Attach");
			rdbtnAttach.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					editorPanel.setState(EditorState.Attach);
				}
			});
			controlPanel.add(rdbtnAttach);

			ButtonGroup group = new ButtonGroup();
			group.add(rdbtnShape);
			group.add(rdbtnAttach);

			rdbtnShape.setSelected(true);

			JButton btnSave = new JButton("Save");
			btnSave.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// Save the edited values
					try {
						Json json = new Json();
						String jsonString = json.prettyPrint(partDesigns);
						File file = new File(PART_DESIGNS_FILE);
						BufferedWriter writer = new BufferedWriter(new FileWriter(file));
						writer.write(jsonString);
						writer.close();

					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			});
			controlPanel.add(btnSave);

			pack();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadImages() throws IOException {
		BufferedImage partsAtlasImage = null;
		partsAtlasImage = ImageIO.read(new File("../lander-android/assets/data/parts.png"));
		partImages = loadAtlas("../lander-android/assets/data/parts.atlas", partsAtlasImage);
	}

	private HashMap<String, BufferedImage> loadAtlas(String atlasFile, BufferedImage atlasImage) throws IOException {
		HashMap<String, BufferedImage> result = new HashMap<>();

		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(atlasFile))));
		try {
			String imageName = null;
			boolean firstImageName = true;
			int x = 0, y = 0, w = 0, h = 0;
			while (true) {
				String line = reader.readLine();
				if (line == null)
					break;

				// Ignore empty lines
				if (line.trim().length() > 0) {
					String[] parts = line.trim().split(":");
					if (parts.length > 1) {
						String[] values = parts[1].split(",");
						if (parts[0].equals("xy")) {
							x = Integer.valueOf(values[0].trim());
							y = Integer.valueOf(values[1].trim());
						} else if (parts[0].equals("size")) {
							w = Integer.valueOf(values[0].trim());
							h = Integer.valueOf(values[1].trim());
						}
					} else {
						// No colon, so this is an image name
						if (imageName != null) {
							// Store the image we have processed
							addImageRegion(result, imageName, x, y, w, h, atlasImage);
						}
						if (firstImageName) {
							firstImageName = false;
						} else {
							imageName = line.trim();
						}
					}
				}
			}
			// Store the last image
			addImageRegion(result, imageName, x, y, w, h, atlasImage);
		} finally {
			reader.close();
		}

		return result;
	}

	private void addImageRegion(HashMap<String, BufferedImage> result, String name, int x, int y, int w, int h,
			BufferedImage source) {
		BufferedImage newImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = (Graphics2D) newImage.getGraphics();
		g2d.drawImage(source, 0, 0, w, h, x, y, x + w, y + h, null);
		result.put(name, newImage);
	}

	public static void main(String[] args) {
		LanderEditor editor = new LanderEditor();
		editor.setVisible(true);
	}
}
