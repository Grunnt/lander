package nl.grunnt.lander.editor;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import nl.grunnt.lander.GameScreen;
import nl.grunnt.lander.lander.part.PartDesign;

import com.badlogic.gdx.math.Vector2;

/**
 * Copyright (c) 2013, Bas van Schoonhoven. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met: Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or other materials provided with the
 * distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

@SuppressWarnings("serial")
public class EditorPanel extends JPanel {

	private float gridSize = 0.1f;
	public static final float EDGE_SNAP_DISTANCE = 0.1f;
	public static final float SIZE_MULTIPLIER = 2f;

	Font smallFont = new Font("Serif", 0, 12);

	BufferedImage image;
	PartDesign design;

	private boolean snapToGrid, snapToSprite, lockX, lockY;

	// Used to remember what point is being dragged, -1 = no dragging is going on
	int dragIndex = -1;

	public enum EditorState {
		Shape, Attach
	};

	EditorState state = EditorState.Shape;

	public float getGridSize() {
		return gridSize;
	}

	public void setGridSize(float gridSize) {
		this.gridSize = gridSize;
	}

	public EditorState getState() {
		return state;
	}

	public void setState(EditorState state) {
		this.state = state;
		dragIndex = -1;
		repaint();
	}

	public boolean isSnapToGrid() {
		return snapToGrid;
	}

	public void setSnapToGrid(boolean snapToGrid) {
		this.snapToGrid = snapToGrid;
	}

	public boolean isSnapToSprite() {
		return snapToSprite;
	}

	public void setSnapToSprite(boolean snapToSprite) {
		this.snapToSprite = snapToSprite;
	}

	public boolean isLockX() {
		return lockX;
	}

	public void setLockX(boolean lockX) {
		this.lockX = lockX;
	}

	public boolean isLockY() {
		return lockY;
	}

	public void setLockY(boolean lockY) {
		this.lockY = lockY;
	}

	public EditorPanel() {
		super();
		setBorder(new LineBorder(new Color(0, 0, 0)));

		addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent me) {

				if (EditorPanel.this.design != null) {
					if (me.getButton() == MouseEvent.BUTTON3) {

						switch (state) {
						case Shape:
							// Right button was clicked, remove closest point
							int closestIndex = getClosestShapePoint(me.getX(), me.getY());
							design.shape.removeIndex(closestIndex);
							repaint();
							break;
						case Attach:
							// Right button was clicked, remove closest point
							if (design.attach.size > 0) {
								closestIndex = getClosestAttachPoint(me.getX(), me.getY());
								design.attach.removeIndex(closestIndex);
								repaint();
							}
							break;
						}

					} else if (me.getButton() == MouseEvent.BUTTON1) {

						switch (state) {
						case Shape:
							// Left button pressed, so start dragging closest (this stops upon releasing the button)
							int closestIndex = getClosestShapePoint(me.getX(), me.getY());
							dragIndex = closestIndex;
							break;
						case Attach:
							// Left button pressed, so start dragging closest (this stops upon releasing the button)
							if (design.attach.size > 0) {
								closestIndex = getClosestAttachPoint(me.getX(), me.getY());
								dragIndex = closestIndex;
								break;
							}
						}

					} else if (me.getButton() == MouseEvent.BUTTON2) {

						switch (state) {
						case Shape:
							// Middle button clicked, so insert new point at the mouse location
							int closestIndex = getClosestShapePoint(me.getX(), me.getY());
							float x = screenToWorldX(me.getX());
							float y = screenToWorldY(me.getY());
							if (snapToGrid) {
								x = x - (x % gridSize);
								y = y - (y % gridSize);
							}
							design.shape.insert(closestIndex, new Vector2(x, y));
							break;
						case Attach:
							// Middle button clicked, so insert new point at the mouse location
							x = screenToWorldX(me.getX());
							y = screenToWorldY(me.getY());
							if (snapToGrid) {
								x = x - (x % gridSize);
								y = y - (y % gridSize);
							}

							design.attach.add(new Vector2(x, y));
							break;
						}
					}
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {

				switch (state) {
				case Shape:
					// Stop dragging the point
					if (dragIndex != -1) {
						// snap to grid
						float x = EditorPanel.this.design.shape.get(dragIndex).x;
						float y = EditorPanel.this.design.shape.get(dragIndex).y;
						if (snapToGrid) {
							x = x - (x % gridSize);
							y = y - (y % gridSize);
						}
						EditorPanel.this.design.shape.get(dragIndex).set(x, y);
					}
					dragIndex = -1;
					repaint();
					break;
				case Attach:
					// Stop dragging the point
					if (dragIndex != -1) {
						// snap to grid
						float x = design.attach.get(dragIndex).x;
						float y = design.attach.get(dragIndex).y;
						if (snapToGrid) {
							x = x - (x % gridSize);
							y = y - (y % gridSize);
						}
						design.attach.get(dragIndex).set(x, y);
					}
					dragIndex = -1;
					repaint();
					break;
				}
			}
		});

		addMouseMotionListener(new MouseMotionAdapter() {

			@Override
			public void mouseDragged(MouseEvent me) {
				if (EditorPanel.this.design != null) {
					switch (state) {
					case Shape:
						if (dragIndex != -1) {
							EditorPanel.this.design.shape.get(dragIndex).set(screenToWorldX(me.getX()),
									screenToWorldY(me.getY()));
							repaint();
						}
						break;
					case Attach:
						if (dragIndex != -1) {
							design.attach.get(dragIndex).set(screenToWorldX(me.getX()), screenToWorldY(me.getY()));
							repaint();
						}
						break;
					}
				}
			}
		});
	}

	/**
	 * Find the point on the shape that is closest to the point passed along.
	 * 
	 * @param x
	 * @return
	 */
	private int getClosestShapePoint(int x, int y) {
		float rx = screenToWorldX(x);
		float ry = screenToWorldY(y);
		int closestIndex = -1;
		double minDistance = Double.MAX_VALUE;
		for (int e = 0; e < design.shape.size; e++) {
			Vector2 v = design.shape.get(e);
			double distance = v.dst(rx, ry);
			if (distance < minDistance) {
				minDistance = distance;
				closestIndex = e;
			}
		}
		return closestIndex;
	}

	private int getClosestAttachPoint(int x, int y) {
		float rx = screenToWorldX(x);
		float ry = screenToWorldY(y);
		int closestIndex = -1;
		float minDistance = Float.MAX_VALUE;
		for (int e = 0; e < design.attach.size; e++) {
			Vector2 v = design.attach.get(e);
			float distance = v.dst(rx, ry);
			if (distance < minDistance) {
				minDistance = distance;
				closestIndex = e;
			}
		}
		return closestIndex;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setDesign(BufferedImage image, PartDesign design) {
		this.image = image;
		this.design = design;
		repaint();
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, getWidth(), getHeight());

		if (image != null) {
			int imageW = (int) (image.getWidth() * SIZE_MULTIPLIER);
			int imageH = (int) (image.getHeight() * SIZE_MULTIPLIER);
			g2d.setColor(Color.white);
			g2d.drawImage(image, getWidth() / 2 - imageW / 2, getHeight() / 2 - imageH / 2, imageW, imageH, null);
		}

		g2d.setFont(smallFont);
		if (design != null) {

			switch (state) {
			case Shape:
				// Draw shape
				g2d.setColor(Color.pink);
				int xp[] = new int[design.shape.size];
				int yp[] = new int[design.shape.size];
				for (int i = 0; i < design.shape.size; i++) {
					xp[i] = worldToScreenX(design.shape.get(i).x);
					yp[i] = worldToScreenY(design.shape.get(i).y);
					g2d.fillOval(xp[i] - 5, yp[i] - 5, 10, 10);
				}
				g2d.drawPolygon(xp, yp, design.shape.size);
				break;
			case Attach:
				// Draw attach points
				g2d.setColor(Color.pink);
				for (int i = 0; i < design.attach.size; i++) {
					int x = worldToScreenX(design.attach.get(i).x);
					int y = worldToScreenY(design.attach.get(i).y);
					g2d.fillOval(x - 5, y - 5, 10, 10);
					g2d.drawString(String.valueOf(i), x + 15, y);
				}
				break;
			}
		}
	}

	public float screenToWorldX(int x) {
		return ((x - getWidth() / 2) / SIZE_MULTIPLIER) * GameScreen.RENDER_TO_WORLD;
	}

	public float screenToWorldY(int y) {
		return ((getHeight() / 2 - y) / SIZE_MULTIPLIER) * GameScreen.RENDER_TO_WORLD;
	}

	public int worldToScreenX(float x) {
		return (int) ((x * GameScreen.WORLD_TO_RENDER) * SIZE_MULTIPLIER + getWidth() / 2);
	}

	public int worldToScreenY(float y) {
		return (int) ((-y * GameScreen.WORLD_TO_RENDER) * SIZE_MULTIPLIER + getHeight() / 2);
	}
}
